﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LoaditPublic.BaseClasses;
using LoaditPublic.BaseClasses.InnerModules;
using LoaditPublic.Data.ModuleIO;
using System.Data;

namespace MachineLearningAccuracy
{
    public class CodeframeItemNumber
    {
        public string CodeId { get; set; }
        public int BaseCount { get; set; }
        public int TruePositives { get; set; }
        public int FalsePositives { get; set; }
        public int FalseNegatives { get; set; }

        public static CodeframeItemNumber CreateNew(string code)
        {
            return new CodeframeItemNumber()
            {
                CodeId = code,
                BaseCount = 0,
                FalseNegatives = 0,
                FalsePositives = 0,
                TruePositives = 0
            };
        }
    }

    public class CalculateAccuracy : TransformationBase
    {
        #region "Member Variables"
        private string _codesColumn = "Improvement Codes";
        private string _ridColumn = "RID";
        private char _characterDelim = ';';
        private Dictionary<string, CodeframeItemNumber> _codesDictionary;
        #endregion

        #region "Construction"
        public CalculateAccuracy() : base("AIAccuracy", "LoaditExtensions" , "This Calculates accuracy metrics for ML Codeit", new Guid("BC7B25C6-807E-4DFD-84D0-84B6F54E0418"))
        {
            SourceRules.Clear();
            SourceRules.Add(new SourceRule((SourceTypeCode)3, SourceMultiplicityType.Exactly, false, 2));
            
        }
        #endregion

        #region "Configuration
        public override ConfigureResult Configure()
        {
            return ConfigureResult.Ok;
        }

        public override ExecuteResult Execute()
        {
            _codesDictionary = new Dictionary<string, CodeframeItemNumber>();

            var correctCodes = Inputs[0].ToDataTable();
            var aiCodes = Inputs[1].ToDataTable();

            DataTable totalNumbersCodes = new DataTable("Total Numbers (Codes)");
            totalNumbersCodes.Columns.Add("Base Total");
            totalNumbersCodes.Columns.Add("Base (In Tagged verbatims)");
            totalNumbersCodes.Columns.Add("True Positives");
            totalNumbersCodes.Columns.Add("False Positives");
            totalNumbersCodes.Columns.Add("False Negatives");


            DataTable totalNumbers = new DataTable("Total Numbers (Verbatims)");
            totalNumbers.Columns.Add("Base Total");
            totalNumbers.Columns.Add("Base (In Tagged verbatims)");
            totalNumbers.Columns.Add("True Positives");
            totalNumbers.Columns.Add("False Positives");
            totalNumbers.Columns.Add("False Negatives");
            
            DataTable codeframeItemBreakDown = new DataTable("Code Breakdown");
            codeframeItemBreakDown.Columns.Add("Code");
            codeframeItemBreakDown.Columns.Add("Base Total");
            codeframeItemBreakDown.Columns.Add("Base (In Tagged verbatims)");
            codeframeItemBreakDown.Columns.Add("True Positives");
            codeframeItemBreakDown.Columns.Add("False Positives");
            codeframeItemBreakDown.Columns.Add("False Negatives");
            codeframeItemBreakDown.Columns.Add("Precision");
            codeframeItemBreakDown.Columns.Add("Recall");
            codeframeItemBreakDown.Columns.Add("F1");
            
            var truePositivesVerbatims = 0;
            var falsePositivesVerbatims = 0;
            var falseNegativesVerbatims = 0;
            var verbatimHandled = false;

            for (var i = 0; i < correctCodes.Rows.Count; i++)
            {
                verbatimHandled = false;

                var real = correctCodes.Rows[i][_codesColumn].ToString().Split(_characterDelim).Where(p => p != "").ToList();
                var predicted = aiCodes.Rows[i][_codesColumn].ToString().Split(_characterDelim).Where(p => p != "").ToList();

                foreach (var p in predicted)
                {
                    if (real.Contains(p))
                        AddTruePositive(p);
                    else { 
                        AddFalsePositive(p);
                        if (!verbatimHandled) {
                            falsePositivesVerbatims += 1;
                            verbatimHandled = true;
                        }
                    }
                }
                foreach (var r in real)
                {
                    AddBaseCount(r);
                    if (!predicted.Contains(r) && predicted.Count > 0) { 
                        AddFalseNegative(r);
                        if (!verbatimHandled) { 
                            falseNegativesVerbatims += 1;
                            verbatimHandled = true;
                        }
                    }
                }

                if (!verbatimHandled && predicted.Count > 0)
                    truePositivesVerbatims += 1;
            }


            var baseCount = 0;
            var truePositives = 0;
            var falsePositives = 0;
            var falseNegatives = 0;

            foreach (var value in _codesDictionary.Values)
            {
                var codeRow = codeframeItemBreakDown.NewRow();
                codeRow["Code"] = value.CodeId;
                codeRow["Base Total"] = value.BaseCount;
                codeRow["Base (In Tagged verbatims)"] = value.TruePositives + value.FalseNegatives;
                codeRow["True Positives"] = value.TruePositives;
                codeRow["False Positives"] = value.FalsePositives;
                codeRow["False Negatives"] = value.FalseNegatives;
                try { 
                    codeRow["Precision"] = (decimal)value.TruePositives / (value.TruePositives + value.FalsePositives);
                    codeRow["Recall"] = (decimal)value.TruePositives / (value.TruePositives + value.FalseNegatives);
                    codeRow["F1"] = (2 * Convert.ToDecimal(codeRow["Precision"].ToString()) * Convert.ToDecimal(codeRow["Recall"].ToString())) / (Convert.ToDecimal(codeRow["Precision"].ToString()) + Convert.ToDecimal(codeRow["Recall"].ToString()));
                } catch (Exception e)
                {
                    codeRow["Precision"] = 0;
                    codeRow["Recall"] = 0;
                    codeRow["F1"] = 0;
                }
                codeframeItemBreakDown.Rows.Add(codeRow);

                baseCount += value.BaseCount;
                truePositives += value.TruePositives;
                falsePositives += value.FalsePositives;
                falseNegatives += value.FalseNegatives;
            }

            var totalCodeRow = totalNumbersCodes.NewRow();
            totalCodeRow["Base Total"] = baseCount;
            totalCodeRow["Base (In Tagged verbatims)"] = truePositives + falseNegatives;
            totalCodeRow["True Positives"] = truePositives;
            totalCodeRow["False Positives"] = falsePositives;
            totalCodeRow["False Negatives"] = falseNegatives;
            totalNumbersCodes.Rows.Add(totalCodeRow);

            var totalVerbatimRow = totalNumbers.NewRow();
            totalVerbatimRow["Base Total"] = correctCodes.Rows.Count;
            totalVerbatimRow["Base (In Tagged verbatims)"] = truePositivesVerbatims + falseNegativesVerbatims + falsePositivesVerbatims;
            totalVerbatimRow["True Positives"] = truePositivesVerbatims;
            totalVerbatimRow["False Positives"] = falsePositivesVerbatims;
            totalVerbatimRow["False Negatives"] = falseNegativesVerbatims;
            totalNumbers.Rows.Add(totalVerbatimRow);

            AddOutput(totalNumbers);
            AddOutput(totalNumbersCodes);
            AddOutput(codeframeItemBreakDown);
            
            return ExecuteResult.Ok;
        }

        private void AddBaseCount(string code)
        {
            if (!_codesDictionary.Keys.Contains(code))
            {
                _codesDictionary[code] = CodeframeItemNumber.CreateNew(code);
            }
            _codesDictionary[code].BaseCount += 1;
        }

        private void AddFalseNegative(string code)
        {
            if (!_codesDictionary.Keys.Contains(code))
            {
                _codesDictionary[code] = CodeframeItemNumber.CreateNew(code);
            }
            _codesDictionary[code].FalseNegatives+= 1;
        }

        private void AddFalsePositive(string code)
        {
            if (!_codesDictionary.Keys.Contains(code))
            {
                _codesDictionary[code] = CodeframeItemNumber.CreateNew(code);
            }
            _codesDictionary[code].FalsePositives += 1;
        }

        private void AddTruePositive(string code)
        {
            if (!_codesDictionary.Keys.Contains(code)) {
                _codesDictionary[code] = CodeframeItemNumber.CreateNew(code);
            }
            _codesDictionary[code].TruePositives += 1;
        }
        #endregion

        #region "Serialisation"
        public override string Serialise()
        {
            return "";
        }
        public override void Deserialise(string customData)
        {
        }
        #endregion
    }
}
